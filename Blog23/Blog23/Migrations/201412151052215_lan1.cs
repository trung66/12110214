namespace Blog23.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Bài Viết", newName: "Posts");
            RenameTable(name: "dbo.Bình luận", newName: "Comments");
            RenameTable(name: "dbo.Thẻ", newName: "Tags");
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.Posts", "Account_AccountID", c => c.Int());
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
            AddForeignKey("dbo.Posts", "Account_AccountID", "dbo.Accounts", "AccountID");
            CreateIndex("dbo.Posts", "Account_AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "Account_AccountID" });
            DropForeignKey("dbo.Posts", "Account_AccountID", "dbo.Accounts");
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Comments", "Author", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Comments", "Title", c => c.String());
            AlterColumn("dbo.Posts", "Body", c => c.String(maxLength: 400));
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false));
            DropColumn("dbo.Posts", "Account_AccountID");
            DropTable("dbo.Accounts");
            RenameTable(name: "dbo.Tags", newName: "Thẻ");
            RenameTable(name: "dbo.Comments", newName: "Bình luận");
            RenameTable(name: "dbo.Posts", newName: "Bài Viết");
        }
    }
}
