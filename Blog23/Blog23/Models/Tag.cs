﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog23.Models
{
    public class Tag
    {
        [Required]
        public int tagID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "Nhập từ 20->500 ký tự", MinimumLength = 10)]
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}