﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog23.Models
{
    public class Post
    {
        [Required]
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "Nhập từ 20->500 ký tự", MinimumLength = 20)]
        public String Title { set; get; }
        [Required]
        [StringLength(10000, ErrorMessage="Tối thiểu 50 ký tự", MinimumLength=50)]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual Account Account { get; set; }
    }
}