﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog23.Models
{
    public class Account
    {
        [Required]
        public int AccountID { get; set; }
        
        [Required]
        [EmailAddress(ErrorMessage = "Email không hợp lệ")]
        public String Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public String Password { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Số lượng ký tự tối đa là 100")]
        public String FirstName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Số lượng ký tự tối đa là 100")]
        public String LastName { get; set; }

        public ICollection<Post> Posts { get; set; }

    }
}