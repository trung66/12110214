﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog23.Models
{
    public class Comment
    {
        [Required]
        public int ID { set; get; }
        [Required]
        public String Title { set; get; }
        [Required]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }
        [Required]
        public String Author { set; get; }
        [Required]
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
        
    }
}