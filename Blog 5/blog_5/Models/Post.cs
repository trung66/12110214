﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace blog_5.Models
{
    public class Post
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [StringLength(500, ErrorMessage = "Số lượng kí tự nằm trong khoảng 20-500", MinimumLength = 20)]
        public String Title { set; get; }

        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [MinLength(50, ErrorMessage = "Bạn phải nhập ít nhất 50 kí tự")]
        public String Body { set; get; }
        public DateTime DateCreate { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }

    }

}