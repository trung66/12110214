﻿using ProductMvc.Models;
namespace ProductMvc.ViewModel
{
    public class CartVM
    {       
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}